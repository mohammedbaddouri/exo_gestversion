J'aime le souvenir de ces époques nues,
Dont Phoebus se plaisait à dorer les statues.
Auteur : Charles P. Baudelaire